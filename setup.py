from distutils.core import setup

setup(
      name='idealplanets',
      version='0.1.0',
      url='https://github.com/bnb32/spring_onset',
      author='Brandon N. Benton',
      description='for running drycore and aquaplanet sims with heat anomalies',
      packages=['idealplanets'],
      package_dir={'idealplanets':'./idealplanets'},
      install_requires=[]
)
